package com.vw.social.greetingservices.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalTime;
import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

@ExtendWith(MockitoExtension.class)
class GreetingServiceTest {

    private static final Locale LOCALE = Locale.GERMAN;

    private static final String GENERAL = "general";
    private static final String MORNING = "morning";
    private static final String AFTERNOON = "afternoon";
    private static final String EVENING = "evening";

    @Mock
    private MessageSource messageSource;

    private GreetingService sut;

    @BeforeEach
    void setup() {
        sut = new GreetingService(messageSource);
    }

    @Test
    void testGetGreetingIntervals() {
        when(messageSource.getMessage("greeting.general", null, LOCALE)).thenReturn(GENERAL);
        when(messageSource.getMessage("greeting.morning", null, LOCALE)).thenReturn(MORNING);
        when(messageSource.getMessage("greeting.afternoon", null, LOCALE)).thenReturn(AFTERNOON);
        when(messageSource.getMessage("greeting.evening", null, LOCALE)).thenReturn(EVENING);

        assertEquals(GENERAL, sut.getGreeting(LOCALE, LocalTime.of(0, 0)));
        assertEquals(GENERAL, sut.getGreeting(LOCALE, LocalTime.of(4, 59)));
        assertEquals(MORNING, sut.getGreeting(LOCALE, LocalTime.of(5, 0)));
        assertEquals(MORNING, sut.getGreeting(LOCALE, LocalTime.of(11, 59)));
        assertEquals(AFTERNOON, sut.getGreeting(LOCALE, LocalTime.of(12, 0)));
        assertEquals(AFTERNOON, sut.getGreeting(LOCALE, LocalTime.of(16, 59)));
        assertEquals(EVENING, sut.getGreeting(LOCALE, LocalTime.of(17, 0)));
        assertEquals(EVENING, sut.getGreeting(LOCALE, LocalTime.of(21, 59)));
        assertEquals(GENERAL, sut.getGreeting(LOCALE, LocalTime.of(22, 0)));
        assertEquals(GENERAL, sut.getGreeting(LOCALE, LocalTime.of(23, 0)));
    }

    @Test
    void testGetGreetingWithEmptyTime() {
        when(messageSource.getMessage("greeting.general", null, LOCALE)).thenReturn(GENERAL);

        final String result = sut.getGreeting(LOCALE, null);

        assertEquals(GENERAL, result);

        verify(messageSource).getMessage("greeting.general", null, LOCALE);
        verifyNoMoreInteractions(messageSource);
    }

    @Test
    void testGetGreetingNoResourceMorning() {
        final String codeMorning = "greeting.morning";
        final LocalTime time = LocalTime.of(11, 59);

        when(messageSource.getMessage(codeMorning, null, LOCALE)).thenThrow(new NoSuchMessageException(codeMorning));
        when(messageSource.getMessage("greeting.general", null, LOCALE)).thenReturn(GENERAL);

        assertEquals(GENERAL, sut.getGreeting(LOCALE, time));

        verify(messageSource).getMessage(codeMorning, null, LOCALE);
        verify(messageSource).getMessage("greeting.general", null, LOCALE);
        verifyNoMoreInteractions(messageSource);
    }

    @Test
    void testGetGreetingNoResourceGeneral() {
        final String code = "greeting.general";
        final LocalTime time = LocalTime.of(0, 0);

        when(messageSource.getMessage(code, null, LOCALE)).thenThrow(new NoSuchMessageException(code, LOCALE));

        assertThrows(NoSuchMessageException.class, () -> sut.getGreeting(LOCALE, time));
    }
}