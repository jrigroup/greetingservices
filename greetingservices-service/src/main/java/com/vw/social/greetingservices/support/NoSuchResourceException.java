package com.vw.social.greetingservices.support;

import java.util.Locale;

public class NoSuchResourceException extends RuntimeException {

    /**
     * Create a new exception.
     * @param locale the locale that was used to search
     */
    public NoSuchResourceException(final Locale locale) {
        super("No resource found for locale '" + locale + "'.");
    }
}