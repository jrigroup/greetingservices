package com.vw.social.greetingservices.service;

import java.time.LocalTime;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class GreetingService {

    public static final String GENERAL = "greeting.general";

    public static final LocalTime time_05 = LocalTime.of(5, 0);
    public static final LocalTime time_12 = LocalTime.of(12, 0);
    public static final LocalTime time_17 = LocalTime.of(17, 0);
    public static final LocalTime time_22 = LocalTime.of(22, 0);

    private final MessageSource messageSource;

    public String getGreeting(final Locale locale, final LocalTime time) {
        String code = GENERAL;

        if (time != null) {
            try {
                if (time.isBefore(time_05)) {
                } else if (time.isBefore(time_12)) {
                    code = "greeting.morning";
                } else if (time.isBefore(time_17)) {
                    code = "greeting.afternoon";
                } else if (time.isBefore(time_22)) {
                    code = "greeting.evening";
                }

                return messageSource.getMessage(code, null, locale);
            } catch (NoSuchMessageException e) {
                if (code.equals(GENERAL)) {
                    throw e;
                }

                log.info(String.format("No resource for code '%s' and locale '%s'.", code, locale));
            }
        }

        return messageSource.getMessage(GENERAL, null, locale);
    }
}