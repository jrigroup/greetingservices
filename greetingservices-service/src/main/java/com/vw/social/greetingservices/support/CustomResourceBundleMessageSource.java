package com.vw.social.greetingservices.support;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.lang.Nullable;

public class CustomResourceBundleMessageSource extends ResourceBundleMessageSource {

    @Nullable
    protected ResourceBundle getResourceBundle(String basename, Locale locale) {
        return Optional.ofNullable(super.getResourceBundle(basename, locale))
            .orElseThrow(() -> new NoSuchResourceException(locale));
    }
}
