package com.vw.social.greetingservices.resp.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CommonError {

    @ApiModelProperty("The error code")
    private String code;

    @ApiModelProperty("The error title")
    private String title;

    @ApiModelProperty("The error detail")
    private String detail;

    @ApiModelProperty("The error source")
    private Source source;

    @ApiModelProperty("The error meta")
    private JsonNode meta;
}