package com.vw.social.greetingservices.config;

import com.vw.social.greetingservices.support.CustomResourceBundleMessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessageSourceConfiguration {

    @Bean
    public ResourceBundleMessageSource messageSource() {

        final ResourceBundleMessageSource source = new CustomResourceBundleMessageSource();
        source.setBasenames("messages/greeting");
        source.setFallbackToSystemLocale(false);

        return source;
    }
}