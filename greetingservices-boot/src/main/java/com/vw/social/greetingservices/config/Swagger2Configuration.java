package com.vw.social.greetingservices.config;

import io.swagger.annotations.SwaggerDefinition.Scheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableSwagger2
class Swagger2Configuration {

    @Value("${build.version}")
    private String buildVersion;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors.basePackage("com.vw.social.greetingservices"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(getApiInfo())
            .protocols(Stream.of(Scheme.HTTP.name()).collect(Collectors.toSet()))
            .useDefaultResponseMessages(false);
    }

    private ApiInfo getApiInfo() {
        final String title = "Greeting services";
        final String description = "This is micro-service Greetin services";
        final String version = getMajorMinorVersion(buildVersion);
        final String termsOfServiceUrl = "";
        final String license = "Apache 2.0";
        final String licenseUrl = "http://www.apache.org/licenses/LICENSE-2.0.html";
        return new ApiInfo(title, description, version, termsOfServiceUrl, getContact(), license, licenseUrl,
                           Collections.emptyList());
    }

    private static Contact getContact() {
        final String name = "Volkswagen Cars & SUVs";
        final String url = "https://www.vw.com/";
        final String email = "support@vw.com";
        return new Contact(name, url, email);
    }

    private static String getMajorMinorVersion(String buildVersion) {
        final Pattern versionPattern = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)(([.-])([a-zA-Z]+(\\d+)?))?$");

        Matcher matcher = versionPattern.matcher(buildVersion);
        if (matcher.matches()) {
            return matcher.group(1) + "." + matcher.group(2);
        } else {
            throw new IllegalArgumentException("Could not parse version string from \"" + buildVersion + "\"");
        }
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder().build();
    }
}