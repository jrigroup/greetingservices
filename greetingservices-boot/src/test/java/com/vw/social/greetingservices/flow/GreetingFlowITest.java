package com.vw.social.greetingservices.flow;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class GreetingFlowITest {

    private static final String URL = "/greeting";

    @Autowired
    private MockMvc mvc;

    @Nested
    @DisplayName("Positive tests")
    class PositiveTest {

        @Test
        @DisplayName("Greeting morning")
        void testMorning() throws Exception {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                            .contentType(MediaType.APPLICATION_JSON)
                            .param("locale", "morning")
                            .param("userTime", "6:00"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value("Wo warst du gestern?"));
        }

        @Test
        @DisplayName("Greeting general")
        void testGeneral() throws Exception {
            mvc.perform(MockMvcRequestBuilders.get(URL)
                            .contentType(MediaType.APPLICATION_JSON)
                            .param("locale", "morning")
                            .param("userTime", "22:00"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.message").value("Haloooo"));
        }
    }

    @Nested
    @DisplayName("Negative tests")
    class NegativeTest {

        @Nested
        @DisplayName("404 - Missing tests")
        class MissingTest {

            @Test
            @DisplayName("No Resource")
            void testNoResource() throws Exception {
                final String locale = "no_resource";

                mvc.perform(MockMvcRequestBuilders.get(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("locale", locale)
                                .param("userTime", "12:00"))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$").exists())
                    .andExpect(jsonPath("$.errors", org.hamcrest.Matchers.hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].title").value(String.format("No resource found for locale '%s'.", locale)));

            }

            @Test
            @DisplayName("No Message")
            void testNoMessage() throws Exception {
                final String locale = "empty";

                mvc.perform(MockMvcRequestBuilders.get(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("locale", locale)
                                .param("userTime", "12:00"))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$").exists())
                    .andExpect(jsonPath("$.errors", org.hamcrest.Matchers.hasSize(1)))
                    .andExpect(jsonPath("$.errors[0].title")
                                   .value(String.format("No message found under code 'greeting.general' for locale '%s'.", locale)));
            }
        }

        @Nested
        @DisplayName("400 - Validation tests")
        class ValidationTest {

            @Test
            @DisplayName("Wrong time format")
            void testNoResource() throws Exception {
                mvc.perform(MockMvcRequestBuilders.get(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("locale", "test")
                                .param("userTime", "12:0a"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$").exists());
            }

            @Test
            @DisplayName("Locale as Null")
            void testLocalNull() throws Exception {
                mvc.perform(MockMvcRequestBuilders.get(URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("userTime", "12:00"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$").exists());
            }
        }
    }
}