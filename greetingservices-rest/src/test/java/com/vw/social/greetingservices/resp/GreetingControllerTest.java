package com.vw.social.greetingservices.resp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.vw.social.greetingservices.resp.api.GreetingResponse;
import com.vw.social.greetingservices.service.GreetingService;
import java.time.LocalTime;
import java.util.Locale;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class GreetingControllerTest {

    @Mock
    private GreetingService greetingService;

    private GreetingController sut;

    @BeforeEach
    void setup() {
        sut = new GreetingController(greetingService);
    }

    @Nested
    @DisplayName("Positive tests")
    class PositiveTest {

        @Test
        @DisplayName("Existing greeting")
        void testReturnMessage() {
            final String message = "Hi";

            when(greetingService.getGreeting(any(Locale.class), any(LocalTime.class))).thenReturn(message);

            final ResponseEntity<GreetingResponse> responseEntity = sut.getGreetingByLocale(Locale.ENGLISH.toString(), LocalTime.now());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertNotNull(responseEntity.getBody());
            assertSame(message, responseEntity.getBody().getMessage());
        }
    }

    @Nested
    @DisplayName("Negative tests")
    class NegativeTest {

        @Test
        @DisplayName("Exception by load from service")
        void testReturnException() {
            final LocalTime time = LocalTime.now();
            final String locale = Locale.ENGLISH.toString();

            when(greetingService.getGreeting(any(Locale.class), any(LocalTime.class))).thenThrow(NullPointerException.class);
            Assertions.assertThrows(NullPointerException.class, () -> sut.getGreetingByLocale(locale, time));
        }
    }
}