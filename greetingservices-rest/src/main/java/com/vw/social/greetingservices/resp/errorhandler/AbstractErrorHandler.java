package com.vw.social.greetingservices.resp.errorhandler;

import com.vw.social.greetingservices.resp.api.CommonError;
import com.vw.social.greetingservices.resp.api.CommonErrorResponse;
import com.vw.social.greetingservices.resp.api.Source;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collections;
import java.util.List;

@Slf4j
public abstract class AbstractErrorHandler {

    private static final String ERROR_LOG = "exception {}: {} with errors {}";

    @Getter
    @Value("${spring.application.name}")
    private String sourceName;

    protected CommonErrorResponse createErrorResponse(final Exception e) {
        return createErrorResponse(e, Collections.singletonList(
            new CommonError(e.getClass().getSimpleName(), e.getMessage(), null, new Source("com.vw.social.greetingservices", sourceName),
                            null)));
    }

    protected CommonErrorResponse createErrorResponse(final Exception e, final List<CommonError> errors) {
        log.error(ERROR_LOG, e.getClass().getSimpleName(), e.getMessage(), errors, e);
        return new CommonErrorResponse(Collections.unmodifiableList(errors));
    }
}