package com.vw.social.greetingservices.resp.errorhandler;

import com.vw.social.greetingservices.resp.api.CommonErrorResponse;
import com.vw.social.greetingservices.support.NoSuchResourceException;
import javax.validation.ConstraintViolationException;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class GlobalErrorHandler extends AbstractErrorHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NoSuchMessageException.class, NoSuchResourceException.class})
    public CommonErrorResponse noHandle(final Exception ex) {
        return createErrorResponse(ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ConstraintViolationException.class, MethodArgumentTypeMismatchException.class})
    public CommonErrorResponse badHandle(final Exception ex) {
        return createErrorResponse(ex);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public CommonErrorResponse handle(Exception ex) {
        return createErrorResponse(ex);
    }
}