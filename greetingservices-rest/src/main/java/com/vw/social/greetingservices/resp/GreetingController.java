package com.vw.social.greetingservices.resp;

import com.vw.social.greetingservices.resp.api.CommonErrorResponse;
import com.vw.social.greetingservices.resp.api.GreetingResponse;
import com.vw.social.greetingservices.service.GreetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.time.LocalTime;
import java.util.Locale;
import javax.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@Api(description = "Greeting API", tags = "greeting support")
@ApiResponses({
                  @ApiResponse(code = 400, message = "Bad request", response = CommonErrorResponse.class),
                  @ApiResponse(code = 404, message = "Not found", response = CommonErrorResponse.class)
              })
@RequestMapping(value = GreetingController.URL_BASE, produces = MediaType.APPLICATION_JSON_VALUE)
public class GreetingController {

    public static final String URL_BASE = "/greeting";

    private final GreetingService greetingService;

    @ApiOperation("Get greeting")
    @ApiResponses(
        @ApiResponse(code = 200, message = "Successfully")
    )
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<GreetingResponse> getGreetingByLocale(
        @RequestParam(required = false) @NotBlank(message = "Invalid locale") final String locale,
        @RequestParam(required = false) @DateTimeFormat(pattern = "H:mm") final LocalTime userTime) {
        return ResponseEntity.ok(new GreetingResponse(greetingService.getGreeting(new Locale(locale), userTime)));
    }
}