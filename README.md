# GREETING SERVICE

JAVA BACKEND TEST

This project was created as a demo for VW.

## Build project

To be able to run Greeting Searvices app is necessary to build it. You will need to run it from the project folder which contains the pom.xml file.

```
mvn package
```

or you can also use

```
mvn install
```

## Run project with java -jar

To run this microservice from a command line in a Terminal window you can you the java -jar command. This is provided your Spring Boot app was packaged as an executable jar file.

```
java -jar greetingservices-boot/target/greetingservices-boot-1.0.0-SNAPSHOT.jar
```

or with profile `local`

```
java -jar -Dspring.profiles.active=local greetingservices-boot/target/greetingservices-boot-1.0.0-SNAPSHOT.jar
```

## ADD NEW COUNTRY

## REST endpoint

**Url:** `/greeting`

**Method:** GET

**Description:** return greeting for `locale` and `userTime`

**Request**

*Url*
```
  http://localhost:8080/greeting?locale=cs_CZ&userTime=13%3A25
```

*Parameter*
* `locale` - a locale *(e.g. en_US, en_GB, cs_CZ, es)*
* `userTime` - user time *(format H:mm)*

*Response* supported response codes:

* 200 - OK
* 400 - Bad request (Error)
* 404 - Not Found (Error)
* 500 - Internal Service Error

**Response for HTTP status 200 (OK)**
```
{
  "message": "string"
}
```
*Fields*
* `message` - a localized greeting message 

**Response for Error**

* HTTP status: e.g. 400
```
{
  "errors": [
    {
      "code": "NoSuchResourceException",
      "title": "No resource found for locale 'sk'.",
      "source": {
        "parameter": "com.vw.social.greetingservices",
        "pointer": "greetingservices"
      }
    }
  ]
}
```
*Fields*
* `code` - exception name
* `title` - error description
* `source.pointer` - name of microservice

## API Documentation

one of the ways is call rest by swagger in explorer on uri `/swagger-ui.html`, [uri](localhost:9000/swagger-ui.html) for `local` profile:

```
localhost:9000/swagger-ui.html
```

![ERD](swagger.png)

## BACKGROUND STORY

Our customer has decided to build a new social network... because apparently, that's what investors nowadays do.
We're currently working on a proof of concept, which, among other features, requires a service that would provide an
appropriate greeting upon every user login. The social network / site is supposed to be used world-wide, hence the
greetings must be in user's preferred language. Last but not least, we don't want to bore our users with the exactly same
greeting each time they login, hence we need a "time sensitive" mechanism which would serve different greeting
depending on the current time of the day.
Your goal is to implement the "greeting service" providing an API to the Ul clients.

### BUSINESS REQUIREMENTS

+ Two distinct operations used to provide a daily greeting are needed
    * Time insensitive - always returns "General purpose" greeting
    * Time sensitive - uses client provided time to choose an appropriate message (see "Time of day  periods"
+ Time of day periods
    * Morning, 05:00 -11:59
    * Afternoon 12:00 - 16:59
    * Evening 17:00 - 21:59
    * General purpose 22:00-04:59
+ Only "General purpose" period is mandatory (for each supported language)
+ When greeting service cannot find a proper greeting message for the given time of the day period, the "General purpose" greeting is used
+ Use locale codes (e.g. "en-US" for American English, "en-GB" for British English) for language specification
+ Please note that not every language necessary has got an expression for each of the specified time periods!
+ Language code is mandatory for each request (i.e. no implicit / default language)

### EXAMPLES


**Given** user with preferred language "cs-CZ"  
**And** the user's system time is 13:25  
**When** time sensitive greeting is requested  
**Then** the greeting response reads "Dobré odpoledne"  

**Given** user with preferred language "cs-CZ"  
**And** the user's system time is 23:23  
**When** time sensitive greeting is requested  
**Then** the greeting response reads "Ahoj"  

**Given** user with preferred language "es"  
**And** the user's system time is 10:30  
**When** time sensitive greeting is requested  
**Then** the greeting response reads "Buenos días"  

**Given** user with preferred language "es"  
**And** the user's system time is 12:01  
**When** time sensitive greeting is requested  
**Then** the greeting response reads "Buenas tardes"  

**Given** user with preferred language "es"  
**And** the user's system time is 18:00  
**When** time sensitive greeting is requested  
**Then** the greeting response reads "Hola"  

### TECHNICAL REQUIREMENTS
* RESTful API
* JSON request & response messages
* Development & runtime platform: Open JDK 11
* Project build tool: Gradle / Maven
* Required frameworks: Spring Boot, Spring MVC, JUnit 5, Slf4J

### ACCEPTANCE CRITERIA
* The service must support British English, American English, Spanish and Czech greetings (working as
specified in **Business Requirements** section)
* All unsupported languages are treated with an appropriate error response message
* Service returns suitable HTTP response code and sensible error message in common / predictable error
situations (e.g. when client doesn't provide language code, when service cannot access some resources it
needs to complete the request, ...)
* Service errors are logged
* Read-only APIs do not require authentication or authorization
* Instructions on how to build and run the service are available in README.md (placed in the root directory of
the project)
* It must be possible to build an "all-in-one" distribution package (a.k.a. "fat jar")
* The (unit) test coverage of the code is above 70% (measured by JaCoCo)
* Expected minimal throughput is 60 requests per second
* Service architecture must support effortless extension of supported languages
* Project is published on GitHub or BitBucket